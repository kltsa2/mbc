<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Home Entity
 *
 * @property string $background_image
 * @property string $background_url
 * @property string $logo_url
 * @property string $intro_header
 * @property string $intro_text
 * @property string $gimage_name
 * @property string $gimage_desc
 * @property string $gimage_url
 * @property int $id
 */
class Home extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
