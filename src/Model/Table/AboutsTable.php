<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Abouts Model
 *
 * @method \App\Model\Entity\About get($primaryKey, $options = [])
 * @method \App\Model\Entity\About newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\About[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\About|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\About patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\About[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\About findOrCreate($search, callable $callback = null, $options = [])
 */
class AboutsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('abouts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('history')
            ->requirePresence('history', 'create')
            ->notEmpty('history');

        $validator
            ->scalar('image_history')
            ->requirePresence('image_history', 'create')
            ->notEmpty('image_history');

        $validator
            ->scalar('goal')
            ->requirePresence('goal', 'create')
            ->notEmpty('goal');

        $validator
            ->scalar('background_image_url')
            ->allowEmpty('background_image_url');

        $validator
            ->scalar('history_image_url')
            ->allowEmpty('history_image_url');

        $validator
            ->scalar('history_header')
            ->allowEmpty('history_header');

        $validator
            ->scalar('goal_header')
            ->allowEmpty('goal_header');

        return $validator;
    }
}
