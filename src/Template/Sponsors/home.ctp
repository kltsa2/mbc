<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sponsor[]|\Cake\Collection\CollectionInterface $sponsors
 */
$this->layout = 'frontend';
?>
<header class="head head-soft" style = "background-image: url('../webroot/img/img1.jpg');">
    <div class="container" style="text-align: center">

        <div class="info">
            <h1 class="lead">
                Sponsors
            </h1>
            <p> </p>
            <p> </p>
            <p class="intro">
                For <b>Ray White Carnegie</b>, <b>Thomson Real Estate</b> and <b>Woodards</b>,
                <br>obtain a form from Michael Thompson prior to engaging an Agent.
            </p>
            <p> </p>
            <p> </p>
        </div>

        <div id="soft-illustration">
            <img src="webroot/img/cake-logo.png" alt="" >
        </div>
    </div>
</header>

<section class="section">
    <div class="container">

        <h2 class="text-center title"><i>Please support our sponsors!</i></h2>

        <?php $count = 0;?>
        <?php foreach ($sponsors as $sponsor): ?>
            <?php if ($count % 2 ==0){ ?>
                <div class="row topspace-2x">
                    <div class="col-sm-6 col-sm-push-6 text-center"><img src="webroot/img/smartHire-1.png" alt=""></div>
                    <div class="col-sm-6 col-sm-pull-6 paddingall bottomspace-xs">
                        <h4><?= h($sponsor->name) ?></h4>
                        <p><?= h($sponsor->description) ?>
                            <a href= <?= h($sponsor->url) ?> target="_blank">More about them &rarr;</a>
                        </p>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row topspace-2x">
                    <div class="col-sm-6 text-center"><img src="../webroot/img/sponsors/Bendigo-1.png" alt=""></div>
                    <div class="col-sm-6 paddingall">
                        <h4>Bendigo Bank</h4>
                        <p>
                        <h4><?= h($sponsor->name) ?></h4>
                        <p><?= h($sponsor->description) ?>
                            <a href= <?= h($sponsor->url) ?> target="_blank">More about them &rarr;</a>
                        </p>
                    </div>
                </div>
            <?php }?>
            <?php $count +=1 ?>
        <?php endforeach ?>
    </div>
</section>