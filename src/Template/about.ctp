<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">

	<title>About - Progressus Bootstrap template</title>
</head>

<body class="page-about">

    <!-- navbar -->
    <div class= "navbar navbar-dual navbar-inverse navbar-fixed-top headroom ontop-now">
        <div class="container">
            <div class="navbar-header">
                <!-- Button for smallest screens -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="index.ctp">
                    <!--                <img src="../webroot/img/logos/test-1.jpg" alt="Progressus HTML5 template">-->
                    <!--                <img src="../webroot/img/logos/mbc.png" alt="Progressus HTML5 template" class="secondary">-->
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a href="index">Home</a></li>
                    <li class="active"><a href="about">About</a></li>
                    <li><a href="venue">Venue</a></li>
                    <li><a href="members">Members</a></li>
                    <li><a href="sponsors">Sponsors</a></li>
                    <li><a href="contact">Contact</a></li>
                    <li><a class="btn btn-rounded" href="signin.html">SIGN IN</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end of navbar -->

	<!-- header -->
	<header class="head-inner">
		<div class="container">
			<div class="row">
				<h1 class="page-title text-center">About Progressus</h1>
				<h2 class="page-lead text-center">Meet the first bullsh*t-free premium Bootstrap template<br>made exclusively for outstanding companies.</h2>
			</div>
		</div>
	</header>
	<!-- end of header -->


    <section class="section section-testimonials jumbotron">
        <div class="container">
            <h2 class="title text-center topspace">History</h2>
            <div id="testimonials-carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#testimonials-carousel" data-slide-to="1"></li>
                    <li data-target="#testimonials-carousel" data-slide-to="2"></li>
                </ol>
                <!--//carousel-indicators-->
                <div class="carousel-inner">
                    <div class="row item active">
                        <div class="col-sm-3 profile">
                            <img src="../webroot/img/img1.jpg" alt="" />
                        </div>
                        <div class="col-sm-9 content">
                            <blockquote>
                                <i class="fa fa-quote-left"></i>
                                <p>History 1</p>
                            </blockquote>
                            <p class="source">Mario Hayes
                                <br/><span class="title">Co-Founder, Superawesome Inc</span>
                            </p>
                        </div>
                        <!--//content-->
                    </div>
                    <!--//item-->
                    <div class="row item">
                        <div class="col-sm-3 profile">
                            <img src="../webroot/img/img2.jpg" alt="" />
                        </div>
                        <div class="col-sm-9 content">
                            <blockquote>
                                <i class="fa fa-quote-left"></i>
                                <p>History2</p>
                            </blockquote>
                            <p class="source">2
                                <br/><span class="title">2</span>
                            </p>
                        </div>
                        <!--//content-->
                    </div>
                    <!--//item-->
                    <div class="row item">
                        <div class="col-sm-3 profile">
                            <img src="../webroot/img/img3.jpg" alt="" />
                        </div>
                        <div class="col-sm-9 content">
                            <blockquote>
                                <i class="fa fa-quote-left"></i>
                                <p>History3</p>
                            </blockquote>
                            <p class="source">3
                                <br/><span class="title">3</span>
                            </p>
                        </div>
                        <!--//content-->
                    </div>
                    <!--//item-->
                </div>
                <!--//carousel-inner-->
            </div>
            <!--//carousel-->
        </div>
    </section>

	<!-- mission section -->
	<section class="section-mission topspace-2x">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-push-1">
					<p class="lead text-center">Our mission is... lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis odit beatae, consequatur adipisci quisquam minima atque, illo, impedit hic obcaecati sed sunt asperiores maiores aperiam aliquid.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- end of mission section -->


	<!-- team section -->
	<section class="section-team topspace">
		<div class="container">

			<h2 class="text-center title">Meet the team</h2>
			<hr>

			<!-- team - row 1 -->
			<div class="row topspace-2x">
				<div class="col-md-6 bottomspace-xs">
					<div class="row">
						<div class="col-sm-3 text-center-xs">
							<a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/1.jpg"></a>
						</div>
						<div class="col-sm-9 text-center-xs">
							<h4>
								Jason Soto
								<span class="person-social"><a href=""><i class="fa fa-twitter"></i> @jsoto34</a></span>
							</h4>

							<p>Jason ipsum dolor sit amet, consectetur adipisicing elit. Atque, cupiditate, numquam. Libero esse odio, laborum, fugiat quis facilis error distinctio provident porro inventore ullam atque vel aut, omnis veritatis dicta.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 bottomspace-xs">
					<div class="row">
						<div class="col-sm-3 text-center-xs"><a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/2.jpg"></a> </div>
						<div class="col-sm-9 text-center-xs">
							<h4>
								Brandy Cooper
								<span class="person-social"><a href=""><i class="fa fa-twitter"></i> @bbbc2</a></span>
							</h4>
							<p>Brandy  non qui ipsum, natus quia. Minima et ex iure incidunt nihil accusantium, repudiandae possimus nulla! Placeat vel eos sequi nesciunt deserunt provident dignissimos voluptate, amet odio veritatis ipsam similique!</p>
						</div>
					</div>
				</div>
			</div>
			<!-- end of row -->

			<!-- team - row 2 -->
			<div class="row topspace-2x">
				<div class="col-md-6 bottomspace-xs">
					<div class="row">
						<div class="col-sm-3 text-center-xs">
							<a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/4.jpg"></a>
						</div>
						<div class="col-sm-9 text-center-xs">
							<h4>
								Lucas Lawrence
								<span class="person-social"><a href=""><i class="fa fa-twitter"></i> @llence</a></span>
							</h4>
							<p>Lucas saepe esse at obcaecati excepturi cumque tempore temporibus, nam magni aspernatur dolorum, nemo error velit expedita quia dolore consequuntur eos, animi ullam ut deserunt quibusdam aliquam, ex. Optio, molestiae.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 bottomspace-xs">
					<div class="row">
						<div class="col-sm-3 text-center-xs"><a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/3.jpg"></a> </div>
						<div class="col-sm-9 text-center-xs">
							<h4>
								Alan Butler
								<span class="person-social"><a href=""><i class="fa fa-twitter"></i> @alanb_</a></span>
							</h4>
							<p>Alan doloribus ducimus, incidunt nostrum consequatur ipsum aperiam architecto veritatis tempora dignissimos quos pariatur illo quasi molestias nesciunt, labore. Numquam ipsam blanditiis eius, tempora ab eum provident consequatur dolor vero</p>
						</div>
					</div>
				</div>
			</div>
			<!-- end of row -->

		</div>
	</section>
	<!-- end of team section -->


	<!-- offices map section -->
	<section class="section-map jumbotron invert topspace-2x">
		<div class="container">
			<h2 class="text-center title">OUR OFFICES</h2>
			<img class="center-block" src="webroot/img/map.png" alt="">
		</div>
	</section>
	<!-- end of offices map section -->


	<!-- investors section -->
	<section class="section-investors topspace-2x">
		<div class="container">

			<h2 class="text-center title">Investors</h2>
			<p class="lead text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, nihil.</p>
			<hr>

			<!-- investors - row 1 -->
			<div class="row topspace-2x">
				<div class="col-md-6 bottomspace-xs">
					<div class="row">
						<div class="col-sm-3 text-center-xs">
							<a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/4.jpg"></a>
						</div>
						<div class="col-sm-9 text-center-xs">
							<h4>
								Jason Soto
								<span class="person-about">ANGEL INVESTOR</span>
							</h4>

							<p>Jason ipsum dolor sit amet, consectetur adipisicing elit. Atque, cupiditate, numquam. Libero esse odio, laborum, fugiat quis facilis error distinctio provident porro inventore ullam atque vel aut, omnis veritatis dicta.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 bottomspace-xs">
					<div class="row">
						<div class="col-sm-3 text-center-xs"><a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/1.jpg"></a> </div>
						<div class="col-sm-9 text-center-xs">
							<h4>
								John Cooper
								<span class="person-about">ANGEL INVESTOR</span>
							</h4>
							<p>Brandy  non qui ipsum, natus quia. Minima et ex iure incidunt nihil accusantium, repudiandae possimus nulla! Placeat vel eos sequi nesciunt deserunt provident dignissimos voluptate, amet odio veritatis ipsam similique!</p>
						</div>
					</div>
				</div>
			</div>
			<!-- end of row -->

			<!-- investors - row 2 -->
			<div class="row topspace-2x">
				<div class="col-md-6 bottomspace-xs">
					<div class="row">
						<div class="col-sm-3 text-center-xs">
							<a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/3.jpg"></a>
						</div>
						<div class="col-sm-9 text-center-xs">
							<h4>
								Lucas Lawrence
								<span class="person-about">ANGEL INVESTOR</span>
							</h4>
							<p>Lucas saepe esse at obcaecati excepturi cumque tempore temporibus, nam magni aspernatur dolorum, nemo error velit expedita quia dolore consequuntur eos, animi ullam ut deserunt quibusdam aliquam, ex. Optio, molestiae.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 bottomspace-xs">
					<div class="row">
						<div class="col-sm-3 text-center-xs"><a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/2.jpg"></a> </div>
						<div class="col-sm-9 text-center-xs">
							<h4>
								Ann Butler
								<span class="person-about">ANGEL INVESTOR</span>
							</h4>
							<p>Alan doloribus ducimus, incidunt nostrum consequatur ipsum aperiam architecto veritatis tempora dignissimos quos pariatur illo quasi molestias nesciunt, labore. Numquam ipsam blanditiis eius, tempora ab eum provident consequatur dolor vero</p>
						</div>
					</div>
				</div>
			</div>
			<!-- end of row -->

		</div>
	</section>

	<!-- call to action section -->
	<section class="cta jumbotron topspace-2x">
		<div class="container">
			<h2>We are hiring!
				<span>Join us today!</span>
			</h2>
			<div class="row topspace">
				<div class="col-sm-9">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat velit voluptatem quae tempora blanditiis sunt fugit facere praesentium vero, est natus, id accusantium ab animi voluptatibus magnam laborum ad nobis nam assumenda dolores quod cumque!</p>
				</div>
				<div class="col-sm-3"><a href="" class="btn btn-block btn-action">SEE VACANCIES</a></div>
			</div>
		</div>
	</section>
	<!-- end of call to action section -->

</body>
</html>
