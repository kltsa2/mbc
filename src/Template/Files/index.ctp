<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\File $file
 */

?>

<h1>Upload File</h1>
<div class="content">
    <?= $this->Flash->render() ?>
    <div class="upload-frm">
        <?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
        <?php echo $this->Form->input('file', ['type' => 'file', 'class' => 'form-control']); ?>
        <?php echo $this->Form->button(__('Upload File'), ['type'=>'submit', 'class' => 'form-controlbtn btn-default']); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>



<div class="content">
    <!-- Table -->
    <table class="table">
        <tr>
            <th width="5%">#</th>
            <th width="20%">Name</th>
            <th width="20%">File</th>
            <th width="12%">Upload Date</th>
            <th width="12%">Actions</th>
        </tr>
        <?php if($filesRowNum > 0):$count = 0; foreach($files as $file): $count++;?>
            <tr>
                <td><?= $this->Number->format($file->id) ?></td>
                <td><?= $file->name ?></td>
                <td><embed src="<?= $file->path.$file->name ?>" width="250px" height="200px"></td>
                <td><?php echo $file->created; ?></td>

                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $file->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $file->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $file->id], ['confirm' => __('Are you sure you want to delete # {0}?', $file->id)]) ?>
                </td>


            </tr>

        <?php endforeach; else:?>
        <tr><td colspan="3">No file(s) found......</td></tr>
        <?php endif; ?>


    </table>
</div>