<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Home $home
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Home'), ['action' => 'edit', $home->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Home'), ['action' => 'delete', $home->id], ['confirm' => __('Are you sure you want to delete # {0}?', $home->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Homes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Home'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="homes view large-9 medium-8 columns content">
    <h3><?= h($home->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Background Image') ?></th>
            <td><?= h($home->background_image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Background Url') ?></th>
            <td><?= h($home->background_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Logo Url') ?></th>
            <td><?= h($home->logo_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Intro Header') ?></th>
            <td><?= h($home->intro_header) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gimage Name') ?></th>
            <td><?= h($home->gimage_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gimage Url') ?></th>
            <td><?= h($home->gimage_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($home->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Intro Text') ?></h4>
        <?= $this->Text->autoParagraph(h($home->intro_text)); ?>
    </div>
    <div class="row">
        <h4><?= __('Gimage Desc') ?></h4>
        <?= $this->Text->autoParagraph(h($home->gimage_desc)); ?>
    </div>
</div>
