<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author"      content="GetTemplate.com">
    <!--
        <title>Progressus Pro - business bootstrap template by GetTemplate</title>

        <link rel="shortcut icon" href="assets/images/gt_favicon.png">

        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:200,400,600|Open+Sans:300,400,700">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        !-->

    <!-- Custom styles for our template -->
    <!--<link rel="stylesheet" href="assets/css/styles.css">!-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <!--<script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>

<![endif]-->
</head>

<body class="page-home">
<!-- navbar -->
<div class= "navbar navbar-dual navbar-inverse navbar-fixed-top headroom ontop-now">
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="index.ctp">
                <!--                <img src="../webroot/img/logos/test-1.jpg" alt="Progressus HTML5 template">-->
                <!--                <img src="../webroot/img/logos/mbc.png" alt="Progressus HTML5 template" class="secondary">-->
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li class="active"><a href="index">Home</a></li>
                <li><a href="about">About</a></li>
                <li><a href="venue">Venue</a></li>
                <li><a href="members">Members</a></li>
                <li><a href="sponsors">Sponsors</a></li>
                <li><a href="contact">Contact</a></li>
                <li><a class="btn btn-rounded" href="signin.html">SIGN IN</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- end of navbar -->

<!-- header -->
<header class="head head-default">
    <div class="container" style="text-align: center">
        <!--            <h1 class="lead text-center">AWESOME BUSINESS TEMPLATE</h1>-->
        <!--			<p class="tagline text-center">PROGRESSUS PRO: Ultimate business site template based on Twitter Bootstrap, by <a href="http://www.gettemplate.com/?utm_source=progressus&amp;utm_medium=template&amp;utm_campaign=progressus">GetTemplate</a></p>-->
        <img src="../img/logos/mbc.png" class="lead text-center"/>
        <!--            <p class="text-center">-->
        <!--				<a class="btn btn-default btn-lg" href="http://www.gettemplate.com/pro/progressus.html?utm_source=head_info&amp;utm_medium=template&amp;utm_campaign=progressus">MORE INFO</a>-->
        <!--				<a class="btn btn-action btn-lg" href="http://www.gettemplate.com/pro/progressus.html?utm_source=head_buy&amp;utm_medium=template&amp;utm_campaign=progressus">BUY NOW FOR $19</a>-->
        <!--			</p>-->
    </div>
</header>
<!-- end of header -->

<!-- intro -->
<section class="section section-intro">
    <div class="container" id = "third">
        <h2 class="text-center">The best place to tell people why they are here</h2>
        <p class="text-center text-muted">
            The difference between involvement and commitment is like an eggs-and-ham breakfast:<br>
            the chicken was involved; the pig was committed.
        </p>
    </div>
</section>
<!-- end of intro-->

<!-- highlights - jumbotron -->
<section class="section jumbotron">
    <div class="container">

        <h3 class="title text-center">
            What's On Events<br>
            <small>Upcoming Events At Our Club.</small>
        </h3>


        <div class="row topspace-2x">
            <figure class="col-md-4 col-sm-8 bottomspace-xs text-center">
                <img src="../webroot/img/img2.jpg" alt="" />
                <h4>Grand Final Eve Family/Friends Day</h4>
                <p class="text-center-xs">Murrumbeena Bowls Club is conducting a family/friends day on Grand Final Eve – Friday 29th September. Bring your family members and friends. Wear your football team’s club colours and have a try at bowls. Coaching will be available and we will run some bowls based events with prizes. A sausage sizzle will be starting at 1.00pm and the bar will be open. Wondering what to do Grand Final Eve? Give bowls a try at Murrumbeena Bowls Club.</p>
            </figure>
            <figure class="col-md-4 col-sm-8 bottomspace-xs text-center">
                <img src="../webroot/img/img1.jpg" alt="" />
                <h4>2017/18 Season</h4>
                <p class="text-center-xs">Murrumbeena Bowls Club is fielding 2 teams in each of the Saturday and Mid Week pennant competitions. On Saturdays, the No. 1 team competes in Division 4, Section 7 : the No. 2 team contests Division 1, Section 3 of the 6-a-side competition. On Tuesdays our No.1 team battles in Division 2, Section 7 and the No. 2 team plays against clubs in Division 4, Section 8. We wish all teams a successful, enjoyable 2017/18 season </p>
            </figure>
            <figure class="col-md-4 col-sm-8  text-center">
                <img src="../webroot/img/img3.jpg" alt="" />
                <h4>Welcome</h4>
                <p class="text-center-xs">Murrumbeena Bowls Club was established in the 1920s. From its earliest beginnings, the club has had a strong sense of community partnership. It has also been a popular social hub, a tradition that continues today.
                    Be surprised. Bowls is more than a sport. We are a true community based club, MBC welcomes all members. The club is constantly growing, so why not be a part of it? </p>
            </figure>

        </div> <!-- /row  -->

    </div>
</section>
<!-- end of highlights -->


<section class="section section-testimonials jumbotron">
    <div class="container">

        <style>
            body {
                font-family: Verdana, sans-serif;
                margin: 0;
            }

            * {
                box-sizing: border-box;
            }

            .row > .column {
                padding: 0 8px;
            }

            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            .column {
                float: left;
                width: 25%;
            }

            /* The Modal (background) */
            .modal {
                display: none;
                position: fixed;
                z-index: 1;
                padding-top: 100px;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background-color: black;
            }

            /* Modal Content */
            .modal-content {
                position: relative;
                background-color: #fefefe;
                margin: auto;
                padding: 0;
                width: 90%;
                max-width: 1200px;
            }

            /* The Close Button */
            .close {
                color: white;
                position: absolute;
                top: 10px;
                right: 25px;
                font-size: 35px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #999;
                text-decoration: none;
                cursor: pointer;
            }

            .mySlides {
                display: none;
            }

            .cursor {
                cursor: pointer
            }

            /* Next & previous buttons */
            .prev,
            .next {
                cursor: pointer;
                position: absolute;
                top: 50%;
                width: auto;
                padding: 16px;
                margin-top: -50px;
                color: white;
                font-weight: bold;
                font-size: 20px;
                transition: 0.6s ease;
                border-radius: 0 3px 3px 0;
                user-select: none;
                -webkit-user-select: none;
            }

            /* Position the "next button" to the right */
            .next {
                right: 0;
                border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover,
            .next:hover {
                background-color: rgba(0, 0, 0, 0.8);
            }

            /* Number text (1/3 etc) */
            .numbertext {
                color: #f2f2f2;
                font-size: 12px;
                padding: 8px 12px;
                position: absolute;
                top: 0;
            }

            img {
                margin-bottom: -4px;
                border-radius: 25px;
            }

            .caption-container {
                text-align: center;
                background-color: black;
                padding: 2px 16px;
                color: white;
            }

            .demo {
                opacity: 0.6;
            }

            .active,
            .demo:hover {
                opacity: 1;
            }

            img.hover-shadow {
                transition: 0.3s
            }

            .hover-shadow:hover {
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
            }
        </style>


        <h2 style="text-align:center">Image Gallery</h2>

        <div class="row">
            <div class="column">
                <img src="../webroot/img/img1.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img src="../webroot/img/img2.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img src="../webroot/img/img3.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img src="../webroot/img/img4.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
            </div>

        </div>

        <div id="myModal" class="modal">
            <span class="close cursor" onclick="closeModal()">&times;</span>
            <div class="modal-content">

                <div class="mySlides">
                    <div class="numbertext">1 / 4</div>
                    <img src="../webroot/img/img1.jpg" style="width:100%">
                </div>

                <div class="mySlides">
                    <div class="numbertext">2 / 4</div>
                    <img src="../webroot/img/img2.jpg" style="width:100%">
                </div>

                <div class="mySlides">
                    <div class="numbertext">3 / 4</div>
                    <img src="../webroot/img/img3.jpg" style="width:100%">
                </div>

                <div class="mySlides">
                    <div class="numbertext">4 / 4</div>
                    <img src="../webroot/img/img4.jpg" style="width:100%">
                </div>

                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>

                <div class="caption-container">
                    <p id="caption"></p>
                </div>


                <div class="column">
                    <img class="demo cursor" src="../webroot/img/img1.jpg" style="width:100%" onclick="currentSlide(1)" alt="Nature and sunrise">
                </div>
                <div class="column">
                    <img class="demo cursor" src="../webroot/img/img2.jpg" style="width:100%" onclick="currentSlide(2)" alt="Trolltunga, Norway">
                </div>
                <div class="column">
                    <img class="demo cursor" src="../webroot/img/img3.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
                </div>
                <div class="column">
                    <img class="demo cursor" src="../webroot/img/img4.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
                </div>

            </div>
        </div>

    </div>
</section>

</body>
</html>
