<?php
/**
  * @var \App\View\AppView $this
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Homes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="homes form large-9 medium-8 columns content">
    <?= $this->Form->create($home) ?>
    <fieldset>
        <legend><?= __('Add Home') ?></legend>
        <?php
            echo $this->Form->control('background_image');
            echo $this->Form->control('background_url');
            echo $this->Form->control('logo_url');
            echo $this->Form->control('intro_header');
            echo $this->Form->control('intro_text');
            echo $this->Form->control('gimage_name');
            echo $this->Form->control('gimage_desc');
            echo $this->Form->control('gimage_url');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
