<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Home[]|\Cake\Collection\CollectionInterface $homes
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Home'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="homes index large-9 medium-8 columns content">
    <h3><?= __('Homes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('background_image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('background_url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('logo_url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('intro_header') ?></th>
                <th scope="col"><?= $this->Paginator->sort('gimage_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('gimage_url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($homes as $home): ?>
            <tr>
                <td><?= h($home->background_image) ?></td>
                <td><?= h($home->background_url) ?></td>
                <td><?= h($home->logo_url) ?></td>
                <td><?= h($home->intro_header) ?></td>
                <td><?= h($home->gimage_name) ?></td>
                <td><?= h($home->gimage_url) ?></td>
                <td><?= $this->Number->format($home->id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $home->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $home->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $home->id], ['confirm' => __('Are you sure you want to delete # {0}?', $home->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
