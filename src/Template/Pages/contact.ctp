<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">

    <title>Contact us - Progressus Bootstrap template</title>
</head>

<body class="page-contact">
<!-- navbar -->
<div class= "navbar navbar-dual navbar-inverse navbar-fixed-top headroom ontop-now">
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="index.ctp">
                <!--                <img src="../webroot/img/logos/test-1.jpg" alt="Progressus HTML5 template">-->
                <!--                <img src="../webroot/img/logos/mbc.png" alt="Progressus HTML5 template" class="secondary">-->
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li><a href="index">Home</a></li>
                <li><a href="about">About</a></li>
                <li><a href="venue">Venue</a></li>
                <li><a href="members">Members</a></li>
                <li><a href="sponsors">Sponsors</a></li>
                <li class="active"><a href="contact">Contact</a></li>
                <li><a class="btn btn-rounded" href="signin.html">SIGN IN</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- end of navbar -->

<!-- header (simple version) -->
<header class="head head-default" style = "background-image: url('../webroot/img/contact.jpg') ; ">
    <div class = "container" style = "...">
        <h2 class = "lead text-center" style = "color: black;">Get In Touch With Us!</h2>
    </div>
</header>
<!-- end of header -->


<!-- form section -->
<section class="section section-contact jumbotron">
    <h2></h2>
    <div class="container">

        <form class="contact-form"  method="POST" name="contactform" action="../../team102/webroot/contact-form-handler.php">
            <fieldset class="topspace">
                <div class="row">
                    <div class="col-sm-4 col-sm-push-2">
                        <div class="form-group">
                            <label for="f-name" class="sr-only">Full name:</label>
                            <input class="form-control" id="f-name" name="f-name" type="text" placeholder="Full name*">
                        </div>
                    </div>
                    <div class="col-sm-4 col-sm-push-2">
                        <div class="form-group">
                            <label for="f-email" class="sr-only">Email:</label>
                            <input class="form-control" id="f-email" name="f-email" type="email" placeholder="Email*">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <div class="row">
                    <div class="col-sm-4 col-sm-push-2">
                        <div class="form-group">
                            <label for="f-reason" class="sr-only">I want to:</label>
                            <div class="select-control">
                                <select size="1" id="f-reason" name="f-reason" class="form-control">
                                    <option value="">I want to:</option>
                                    <option value="quote">get price qoute</option>
                                    <option value="partner">discuss partnership opportunities</option>
                                    <option value="support">get customer support</option>
                                    <option value="other">other</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-sm-push-2">
                        <div class="form-group">
                            <label for="f-subject" class="sr-only">Subject</label>
                            <input class="form-control" id="f-subject" name="f-subject" type="text" placeholder="Subject*" >
                        </div>
                    </div>
            </fieldset>
            <fieldset class="topspace">
                <div class="row">
                    <div class="col-sm-8 col-sm-push-2">
                        <div class="form-group">
                            <label for="f-message" class="sr-only">Message</label>
                            <textarea class="form-control" id="f-message" name="f-message" rows="5" cols="40" placeholder="Message*"></textarea>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary">Get in touch</button>
                        </div>
                    </div>
                </div>
            </fieldset>


        </form>
    </div>
</section>
<!-- end of form section -->


<section class="section section-address">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-sm-push-2">
                <h4>Address:</h4>
                <address>
                    2002 Holcombe Boulevard, Houston, TX 77030, USA
                </address>
            </div>
            <div class="col-sm-3 col-sm-push-2">
                <h4>Phone, email:</h4>
                <address>
                    (713) 791-1414<br>
                    <a href="">hello@company.com</a>
                </address>
            </div>
            <div class="col-sm-2 col-sm-push-2">
                <h4>Social:</h4>
                <ul class="social-icons">
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    <li><a href=""><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="section bottomspace-0">
    <div class = "container" id ="map"></div>
</section>


<script>
    function myMap() {
        var mapOptions = {
            center: new google.maps.LatLng(-37.88717 , 145.06598),
            zoom: 20,
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    }
</script>
 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYwlS_DyZonvceixW_1xBibQFqD8Vsl4E&callback=myMap"
            async defer></script>


</body>
</html>
