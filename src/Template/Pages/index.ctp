<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="GetTemplate.com">
<!--
	<title>Progressus Pro - business bootstrap template by GetTemplate</title>

	<link rel="shortcut icon" href="assets/images/gt_favicon.png">

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:200,400,600|Open+Sans:300,400,700">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	!-->

	<!-- Custom styles for our template -->
	<!--<link rel="stylesheet" href="assets/css/styles.css">!-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<!--<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>

	<![endif]-->
</head>

<body class="page-home">
    <!-- navbar -->
    <div class= "navbar navbar-dual navbar-inverse navbar-fixed-top headroom ontop-now">
        <div class="container">
            <div class="navbar-header">
                <!-- Button for smallest screens -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="index.ctp">
                    <!--                <img src="../webroot/img/logos/test-1.jpg" alt="Progressus HTML5 template">-->
                    <!--                <img src="../webroot/img/logos/mbc.png" alt="Progressus HTML5 template" class="secondary">-->
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav pull-right">
                    <li class="active"><a href="index">Home</a></li>
                    <li><a href="about">About</a></li>
                    <li><a href="venue">Venue</a></li>
                    <li><a href="members">Members</a></li>
                    <li><a href="sponsors">Sponsors</a></li>
                    <li><a href="contact">Contact</a></li>
                    <li><a class="btn btn-rounded" href="signin.html">SIGN IN</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end of navbar -->

	<!-- header -->
	<header class="head head-default">
		<div class="container" style="text-align: center">
<!--            <h1 class="lead text-center">AWESOME BUSINESS TEMPLATE</h1>-->
<!--			<p class="tagline text-center">PROGRESSUS PRO: Ultimate business site template based on Twitter Bootstrap, by <a href="http://www.gettemplate.com/?utm_source=progressus&amp;utm_medium=template&amp;utm_campaign=progressus">GetTemplate</a></p>-->
            <img src="../img/logos/mbc.png" class="lead text-center"/>
<!--            <p class="text-center">-->
<!--				<a class="btn btn-default btn-lg" href="http://www.gettemplate.com/pro/progressus.html?utm_source=head_info&amp;utm_medium=template&amp;utm_campaign=progressus">MORE INFO</a>-->
<!--				<a class="btn btn-action btn-lg" href="http://www.gettemplate.com/pro/progressus.html?utm_source=head_buy&amp;utm_medium=template&amp;utm_campaign=progressus">BUY NOW FOR $19</a>-->
<!--			</p>-->
		</div>
	</header>
	<!-- end of header -->

	<!-- intro -->
	<section class="section section-intro">
		<div class="container" id = "third">
			<h2 class="text-center">The best place to tell people why they are here</h2>
			<p class="text-center text-muted">
				The difference between involvement and commitment is like an eggs-and-ham breakfast:<br>
				the chicken was involved; the pig was committed.
			</p>
		</div>
	</section>
	<!-- end of intro-->

	<!-- highlights - jumbotron -->
	<section class="section jumbotron">
		<div class="container">

			<h3 class="title text-center">
				What's On Events<br>
				<small>Upcoming Events At Our Club.</small>
			</h3>


			<div class="row topspace-2x">
				<figure class="col-md-3 col-sm-6 bottomspace-xs text-center">
					<i class="fa fa-cogs fa-4x color-accent"></i>
					<h4>Bootstrap-powered</h4>
					<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque aliquid adipisci aspernatur. Soluta quisquam dignissimos earum quasi voluptate. Amet, dignissimos, tenetur vitae dolor quam iusto assumenda hic reprehenderit?</p>
				</figure>
				<figure class="col-md-3 col-sm-6 bottomspace-xs text-center">
					<i class="fa fa-paper-plane-o fa-4x color-accent"></i>
					<h4>Fat-free</h4>
					<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, commodi, sequi quis ad fugit omnis cumque a libero error nesciunt molestiae repellat quos perferendis numquam quibusdam rerum repellendus laboriosam reprehenderit! </p>
				</figure>
				<figure class="col-md-3 col-sm-6 bottomspace-xs text-center">
					<i class="fa fa-cc fa-4x color-accent"></i>
					<h4>Creative Commons</h4>
					<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, vitae, perferendis, perspiciatis nobis voluptate quod illum soluta minima ipsam ratione quia numquam eveniet eum reprehenderit dolorem dicta nesciunt corporis?</p>
				</figure>
				<figure class="col-md-3 col-sm-6 bottomspace-xs text-center">
					<i class="fa fa-life-ring fa-4x color-accent"></i>
					<h4>Author's support</h4>
					<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, excepturi, maiores, dolorem quasi reprehenderit illo accusamus nulla minima repudiandae quas ducimus reiciendis odio sequi atque temporibus facere corporis eos expedita? </p>
				</figure>
			</div> <!-- /row  -->

		</div>
	</section>
	<!-- end of highlights -->

	<!-- FAQ -->
	<section class="section section-faq">
		<div class="container">

			<h2 class="text-center title">Team Scores</h2>
			<br>

			<div class="row">
				<div class="col-sm-6">
					<h4><b>Which code editor would you recommend?</b></h4>
					<p>I'd highly recommend you <a href="http://www.sublimetext.com/">Sublime Text</a> - a free to try text editor which I'm using daily. Awesome tool!</p>
				</div>
				<div class="col-sm-6">
					<h4><b>Nice header. Where do I find more images like that one?</b></h4>
					<p>
						Well, there are thousands of stock art galleries, but personally,
						I prefer to use photos from these sites: <a href="http://unsplash.com">Unsplash.com</a>
						and <a href="http://www.flickr.com/creativecommons/by-2.0/tags/">Flickr - Creative Commons</a></p>
				</div>
			</div> <!-- /row -->

			<div class="row topspace">
				<div class="col-sm-6">
					<h4><b>Can I use it to build a site for my client?</b></h4>
					<p>
						Yes, you can. You may use this template for any purpose, just don't forget about the <a href="http://creativecommons.org/licenses/by/3.0/">license</a>,
						which says: "You must give appropriate credit", i.e. you must provide the name of the creator and a link to the original template in your work.
					</p>
				</div>
				<div class="col-sm-6">
					<h4><b>Can you customize this template for me?</b></h4>
					<p>Yes, I can. Please drop me a line to sergey-at-pozhilov.com and describe your needs in details. Please note, my services are not cheap.</p>
				</div>
			</div> <!-- /row -->
		</div>
	</section>
	<!-- end of FAQ -->

    <section class="section section-testimonials jumbotron">
        <div class="container">

            <style>
                body {
                    font-family: Verdana, sans-serif;
                    margin: 0;
                }

                * {
                    box-sizing: border-box;
                }

                .row > .column {
                    padding: 0 8px;
                }

                .row:after {
                    content: "";
                    display: table;
                    clear: both;
                }

                .column {
                    float: left;
                    width: 25%;
                }

                /* The Modal (background) */
                .modal {
                    display: none;
                    position: fixed;
                    z-index: 1;
                    padding-top: 100px;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                    overflow: auto;
                    background-color: black;
                }

                /* Modal Content */
                .modal-content {
                    position: relative;
                    background-color: #fefefe;
                    margin: auto;
                    padding: 0;
                    width: 90%;
                    max-width: 1200px;
                }

                /* The Close Button */
                .close {
                    color: white;
                    position: absolute;
                    top: 10px;
                    right: 25px;
                    font-size: 35px;
                    font-weight: bold;
                }

                .close:hover,
                .close:focus {
                    color: #999;
                    text-decoration: none;
                    cursor: pointer;
                }

                .mySlides {
                    display: none;
                }

                .cursor {
                    cursor: pointer
                }

                /* Next & previous buttons */
                .prev,
                .next {
                    cursor: pointer;
                    position: absolute;
                    top: 50%;
                    width: auto;
                    padding: 16px;
                    margin-top: -50px;
                    color: white;
                    font-weight: bold;
                    font-size: 20px;
                    transition: 0.6s ease;
                    border-radius: 0 3px 3px 0;
                    user-select: none;
                    -webkit-user-select: none;
                }

                /* Position the "next button" to the right */
                .next {
                    right: 0;
                    border-radius: 3px 0 0 3px;
                }

                /* On hover, add a black background color with a little bit see-through */
                .prev:hover,
                .next:hover {
                    background-color: rgba(0, 0, 0, 0.8);
                }

                /* Number text (1/3 etc) */
                .numbertext {
                    color: #f2f2f2;
                    font-size: 12px;
                    padding: 8px 12px;
                    position: absolute;
                    top: 0;
                }

                img {
                    margin-bottom: -4px;
                    border-radius: 25px;
                }

                .caption-container {
                    text-align: center;
                    background-color: black;
                    padding: 2px 16px;
                    color: white;
                }

                .demo {
                    opacity: 0.6;
                }

                .active,
                .demo:hover {
                    opacity: 1;
                }

                img.hover-shadow {
                    transition: 0.3s
                }

                .hover-shadow:hover {
                    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
                }
            </style>


            <h2 style="text-align:center">Image Gallery</h2>

            <div class="row">
                <div class="column">
                    <img src="../webroot/img/img1.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
                </div>
                <div class="column">
                    <img src="../webroot/img/img2.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
                </div>
                <div class="column">
                    <img src="../webroot/img/img3.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
                </div>
                <div class="column">
                    <img src="../webroot/img/img4.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
                </div>

            </div>

            <div id="myModal" class="modal">
                <span class="close cursor" onclick="closeModal()">&times;</span>
                <div class="modal-content">

                    <div class="mySlides">
                        <div class="numbertext">1 / 4</div>
                        <img src="../webroot/img/img1.jpg" style="width:100%">
                    </div>

                    <div class="mySlides">
                        <div class="numbertext">2 / 4</div>
                        <img src="../webroot/img/img2.jpg" style="width:100%">
                    </div>

                    <div class="mySlides">
                        <div class="numbertext">3 / 4</div>
                        <img src="../webroot/img/img3.jpg" style="width:100%">
                    </div>

                    <div class="mySlides">
                        <div class="numbertext">4 / 4</div>
                        <img src="../webroot/img/img4.jpg" style="width:100%">
                    </div>

                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>

                    <div class="caption-container">
                        <p id="caption"></p>
                    </div>


                    <div class="column">
                        <img class="demo cursor" src="../webroot/img/img1.jpg" style="width:100%" onclick="currentSlide(1)" alt="Nature and sunrise">
                    </div>
                    <div class="column">
                        <img class="demo cursor" src="../webroot/img/img2.jpg" style="width:100%" onclick="currentSlide(2)" alt="Trolltunga, Norway">
                    </div>
                    <div class="column">
                        <img class="demo cursor" src="../webroot/img/img3.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
                    </div>
                    <div class="column">
                        <img class="demo cursor" src="../webroot/img/img4.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
                    </div>

                </div>
            </div>

            <script>
                function openModal() {
                    document.getElementById('myModal').style.display = "block";
                }

                function closeModal() {
                    document.getElementById('myModal').style.display = "none";
                }

                var slideIndex = 1;
                showSlides(slideIndex);

                function plusSlides(n) {
                    showSlides(slideIndex += n);
                }

                function currentSlide(n) {
                    showSlides(slideIndex = n);
                }

                function showSlides(n) {
                    var i;
                    var slides = document.getElementsByClassName("mySlides");
                    var dots = document.getElementsByClassName("demo");
                    var captionText = document.getElementById("caption");
                    if (n > slides.length) {slideIndex = 1}
                    if (n < 1) {slideIndex = slides.length}
                    for (i = 0; i < slides.length; i++) {
                        slides[i].style.display = "none";
                    }
                    for (i = 0; i < dots.length; i++) {
                        dots[i].className = dots[i].className.replace(" active", "");
                    }
                    slides[slideIndex-1].style.display = "block";
                    dots[slideIndex-1].className += " active";
                    captionText.innerHTML = dots[slideIndex-1].alt;
                }
            </script>



        </div>
    </section>



    <!--
    <section class="section section-cta">
        <div class="container">
            <h2>Get started
                <span>Start your free, 30 day trial today!</span>
            </h2>
            <div class="row topspace">
                <div class="col-sm-9">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat velit voluptatem quae tempora blanditiis sunt fugit facere praesentium vero, est natus, id accusantium ab animi voluptatibus magnam laborum ad nobis nam assumenda dolores quod cumque!</p>
                </div>
                <div class="col-sm-3"><a href="" class="btn btn-block btn-action">Start free trial</a></div>
            </div>
        </div>
    </section>
    !-->
</body>
</html>
