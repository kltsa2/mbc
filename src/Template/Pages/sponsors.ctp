<!DOCTYPE html>
<html lang="en">
<head>
<!--	<meta charset="utf-8">-->
<!--	<meta name="viewport"    content="width=device-width, initial-scale=1.0">-->
<!--	<meta name="description" content="">-->
<!--	<meta name="author"      content="GetTemplate.com">-->
<!---->
<!--	<title>Landing page for a startup - Progressus Pro - bootstrap template by GetTemplate</title>-->
<!---->
<!--	<link rel="shortcut icon" href="assets/images/gt_favicon.png">-->
<!---->
<!--	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:200,400,600|Open+Sans:300,400,700">-->
<!--	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">-->
<!--	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">-->

	<!-- Custom styles for our template -->
<!--    <link rel="stylesheet" href="assets/css/styles.css">-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <!--<script src="assets/js/html5shiv.js"></script>-->
    <!--<script src="assets/js/respond.min.js"></script>-->
	<![endif]-->
</head>

<body class="page-home">

	<!-- navbar -->
    <div class= "navbar navbar-dual navbar-inverse navbar-fixed-top headroom ontop-now">
        <div class="container">
            <div class="navbar-header">
                <!-- Button for smallest screens -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="index.ctp">
                    <!-- <img src="../webroot/img/logos/test-1.jpg" alt="Progressus HTML5 template">-->
                    <!-- <img src="../webroot/img/logos/mbc.png" alt="Progressus HTML5 template" class="secondary">-->
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a href="index">Home</a></li>
                    <li><a href="about">About</a></li>
                    <li><a href="venue">Venue</a></li>
                    <li><a href="members">Members</a></li>
                    <li class="active"><a href="sponsors">Sponsors</a></li>
                    <li><a href="contact">Contact</a></li>
                    <li><a class="btn btn-rounded" href="signin.html">SIGN IN</a></li>
                </ul>
            </div>
        </div>
    </div>
	<!-- end of navbar -->

	<!-- header -->
	<header class="head head-soft" style ="background-image: url('../webroot/img/bg_header.jpg');">
		<div class="container" style="text-align: center">

			<div class="info">
<!--				<h1 class="lead">-->
<!--					InnoZ-->
<!--					<span>DSE which works perfectly for small teams</span>-->
<!--				</h1>-->
<!--				<p class="intro">-->
<!--					Running a software business? Building a web application?<br>-->
<!--					This template is for you! It'll work perfectly either for your home or landing page.<br>-->
<!--					You'll just need to replace the screenshot below by your own one, and edit textual content.-->
<!--				</p>-->
<!---->
<!--				<form class="form-inline" role="form">-->
<!--					<div class="form-group"><input type="text" class="form-control" id="exampleInputEmail2" placeholder="Your Name"></div>-->
<!--					<div class="form-group"><input type="text" class="form-control" id="exampleInputPassword2" placeholder="Your Email"></div>-->
<!--					<button type="submit" class="btn btn-action">Sign Up - it's free</button>-->
<!--				</form>-->
                    <h1 class="lead">
                        Sponsors
                    </h1>
                    <p> </p>
                    <p> </p>
                    <p class="intro">
                        For <b>Ray White Carnegie</b>, <b>Thomson Real Estate</b> and <b>Woodards</b>,
                        <br>obtain a form from Michael Thompson prior to engaging an Agent.
                    </p>
                    <p> </p>
                    <p> </p>
			</div>

			<div id="soft-illustration">
				<img src="../img/logos/test.jpg" alt="" >
			</div>
		</div>
	</header>

<!--	<div class="opaque" style="float:left; width:100%;">-->
<!---->
<!--		<!-- intro -->-->
<!--		<section class="section section-intro">-->
<!--			<div class="container">-->
<!--				<h2 class="text-center">The only solution you need for your team!</h2>-->
<!--				<div class="row">-->
<!--					<div class="col-sm-10 col-sm-push-1">-->
<!--						<p class="text-center text-muted"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae eum quidem dolor praesentium nam, natus, mollitia corrupti, consequuntur itaque dolorem voluptas nemo possimus sint dignissimos nesciunt numquam officiis aut tenetur. </p>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</section>-->
<!--		<!-- end of intro-->
<!---->
<!--		<!-- testimonials section -->
<!--		<section class="section section-testimonials jumbotron">-->
<!--			<div class="container">-->
<!--			<h2 class="title text-center topspace">What people are saying about us?</h2>-->
<!--			<div id="testimonials-carousel" class="carousel slide" data-ride="carousel">-->
<!--				<ol class="carousel-indicators">-->
<!--					<li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>-->
<!--					<li data-target="#testimonials-carousel" data-slide-to="1"></li>-->
<!--					<li data-target="#testimonials-carousel" data-slide-to="2"></li>-->
<!--				</ol>-->
<!--				<!--//carousel-indicators-->
<!--				<div class="carousel-inner">-->
<!--					<div class="row item active">-->
<!--						<div class="col-sm-3 profile">-->
<!--							<img src="assets/images/people/1.jpg" alt="" />-->
<!--						</div>-->
<!--						<div class="col-sm-9 content">-->
<!--							<blockquote>-->
<!--								<i class="fa fa-quote-left"></i>-->
<!--								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam deserunt accusamus soluta maiores alias, aliquid ipsum nobis laudantium qui, eos architecto accusantium doloremque dolorem quia asperiores voluptate pariatur. Laboriosam, ullam.</p>-->
<!--							</blockquote>-->
<!--							<p class="source">Mario Hayes-->
<!--								<br/><span class="title">Co-Founder, Superawesome Inc</span>-->
<!--							</p>-->
<!--						</div>-->
<!--						<!--//content-->
<!--					</div>-->
<!--					<!--//item-->
<!--					<div class="row item">-->
<!--						<div class="col-sm-3 profile">-->
<!--							<img src="assets/images/people/2.jpg" alt="" />-->
<!--						</div>-->
<!--						<div class="col-sm-9 content">-->
<!--							<blockquote>-->
<!--								<i class="fa fa-quote-left"></i>-->
<!--								<p>Ipsa nemo, minus perspiciatis harum at, repudiandae quae aliquam quo? Ullam laborum unde corporis eos ipsa esse necessitatibus in natus atque labore delectus, aperiam perferendis, assumenda iusto qui cum dolor!</p>-->
<!--							</blockquote>-->
<!--							<p class="source">Erin Mendoza-->
<!--								<br/><span class="title">Entrepreneur, GT corp</span>-->
<!--							</p>-->
<!--						</div>-->
<!--						<!--//content-->
<!--					</div>-->
<!--					<!--//item-->
<!--					<div class="row item">-->
<!--						<div class="col-sm-3 profile">-->
<!--							<img src="assets/images/people/3.jpg" alt="" />-->
<!--						</div>-->
<!--						<div class="col-sm-9 content">-->
<!--							<blockquote>-->
<!--								<i class="fa fa-quote-left"></i>-->
<!--								<p>Voluptatibus laboriosam pariatur doloribus repudiandae blanditiis cum quibusdam similique nisi nemo labore eos sed quasi quia, assumenda, ratione sit provident beatae hic eius velit neque magni distinctio ab quaerat ipsa!</p>-->
<!--							</blockquote>-->
<!--							<p class="source">Dwight Ray-->
<!--								<br/><span class="title">CTO, Drave</span>-->
<!--							</p>-->
<!--						</div>-->
<!--						<!--//content-->
<!--					</div>-->
<!--					<!--//item-->
<!--				</div>-->
<!--				<!--//carousel-inner-->
<!--			</div>-->
<!--			<!--//carousel-->
<!--			</div>-->
<!--		</section>-->
<!--		<!-- end of testimonials -->
<!---->
<!--	</div>-->

	<!-- features section-->
	<section class="section">
		<div class="container">

			<h2 class="text-center title"><i>Please support our sponsors!</i></h2>

			<div class="row topspace-2x">
				<div class="col-sm-6 col-sm-push-6 text-center"><img src="../webroot/img/sponsors/astoriaHonda-1.png" alt=""></div>
				<div class="col-sm-6 col-sm-pull-6 paddingall bottomspace-xs">
					<h4>Astoria Honda Centre</h4>
					<p>If a member or friend is buying a new or used car, mention MBC Inc to David Ferguson.
                        <br>davidf@astoriahonda.com.au
                        <br>961 North Road Bentleigh East VIC3165
                        <br>PO Box 406  Bentleigh East VIC3165
                        <br><b>Sales</b> Telephone (03) 9579 1988
                        <br><b>Service & Parts</b> Telephone (03) 9579 1811
                        <br>Facisimile (03) 95637042
                        <br>ABN 53 004 820 222
                        <br>
                        <a href="http://www.astoriahonda.com.au" target="_blank">More about them &rarr;</a>
					</p>
				</div>
			</div>
			<div class="row topspace-2x">
				<div class="col-sm-6 text-center"><img src="../webroot/img/sponsors/Bendigo-1.png" alt=""></div>
				<div class="col-sm-6 paddingall">
					<h4>Bendigo Bank</h4>
					<p>
                        <h5>Murrumbeena <b>Community Bank</b> Branch.</h5>
                        At Bendigo Bank we are here to serve you, not the other way around.
                        <br>Everything we do is designed to make things easier for you. That is why we offer the convenience of banking over the phone and computer, as well as traditional friendly face-to-face service.
                        <br>We also offer a full range of personal and business banking products and services.
                        <br>For more information call into 436 Neerim Road, Murrumbeena or phone 9568 8166.
						<br>
						<a href="https://www.bendigobank.com.au" target="_blank">More about them &rarr;</a>
					</p>
				</div>
			</div>
			<div class="row topspace-2x">
				<div class="col-sm-6 col-sm-push-6 text-center"><img src="../webroot/img/sponsors/djp-1.png" alt=""></div>
				<div class="col-sm-6 col-sm-pull-6 paddingall bottomspace-xs">
					<h4>David Jones Pharmacy</h4>
					<p>
						Club Members receive <b>5% discount</b> for non-prescription items. Just let our friendly staff know.
                        <br>Visit us at 448 Neerim Road Murrumbeena,
                        <br>Weekdays 9am-6:30pm, Weekend 9am-1pm
                        <br>Contact us at
                        <br>Telephone 9568 6677
                        <br>Fax 9568 7737
						<br>
						<a href="https://www.davidjonespharmacy.com.au" target="_blank">More about them &rarr;</a>
					</p>
				</div>
			</div>
			<div class="row topspace-2x">
				<div class="col-sm-6 text-center"><img src="../webroot/img/sponsors/kingstonfunerals-1.jpg" alt=""></div>
				<div class="col-sm-6 paddingall">
					<h4>Kingston Funeral Services</h4>
					<p>
                        <h5><i>Putting Families First</i></h5>
                        x All arrangements taken care of.
                        <br>x 24 hour care - 7 days, 365 days of the year.
                        <br>x Phone (03) 9585 2822
						<br>
						<a href="https://www.kingstonfunerals.com.au" target="_blank">More about them &rarr;</a>
					</p>
				</div>
			</div>
            <div class="row topspace-2x">
                <div class="col-sm-6 col-sm-push-6 text-center"><img src="../webroot/img/sponsors/LeighOak-1.jpg" alt=""></div>
                <div class="col-sm-6 col-sm-pull-6 paddingall bottomspace-xs">
                    <h4>Leighoak Club</h4>
                    <p>
                        <i>*Conditions apply. Offer & price subject to change.</i>
                        <br>
                        <br><u>Seniors Special</u>
                        <br>Available 7 days a week lunch or dinner.
                        <br>2-Course $14.00 or 3-Course $16.00.
                        <br>
                        <br><u>Parma Night Every Sun, Mon & Tues Night</u>
                        <br>Enjoy our delivious $15.00 Parma's
                        <br>
                        <br><u>Mixed Grill Night Every Wed & Thurs Night</u>
                        <br>$23.00 Mixed Grills. You won't be disappointed!
                        <br>
                        <br><u>Jumbo Surf & Turf Night Every Saturday</u>
                        <br>Our scrumptions Surf & Turf steaks are only $25.00
                        <br>
                        <br>
                        <a href="https://www.leighoak.com.au" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>
            <div class="row topspace-2x">
                <div class="col-sm-6 text-center"><img src="../webroot/img/sponsors/lobsterCave-1.jpg" alt=""></div>
                <div class="col-sm-6 paddingall">
                    <h4>Lobster Cave</h4>
                    <p>
                        <i>Specials</i>
                        <br>3 Course Weekday Luncheon @ $19.90
                        <br>3 Course Seafood Indulgence @ $29.90 (excluding Sat)
                        <br><b>BYO WINE</b>
                        <br>18 North Concourse, Beaumaris
                        <br>Telephone 9589 2148
                        <br>
                        <a href="https://www.lobstercave.com.au" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>
            <div class="row topspace-2x">
                <div class="col-sm-6 col-sm-push-6 text-center"><img src="../webroot/img/sponsors/minuteman-1.png" alt=""></div>
                <div class="col-sm-6 col-sm-pull-6 paddingall bottomspace-xs">
                    <h4>Minuteman Press Dandenong</h4>
                    <p>
                        F3 171-173 Cheltenham Road, Dandenong VIC3175
                        <br>Phone (03) 9793 6311
                        <br>Fax (03) 9791 6119
                        <br>dandysales@minutemanpress.com
                        <br>
                        <a href="https://www.dandenong.minutepress.com.au" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>
            <div class="row topspace-2x">
                <div class="col-sm-6 text-center"><img src="../webroot/img/sponsors/mossandmaple-1.png" alt=""></div>
                <div class="col-sm-6 paddingall">
                    <h4>Moss & Maple</h4>
                    <p>
                        x Corporate/office flowers
                        <br>x Artificial Weddings
                        <br>x Gifts & Plants
                        <br>
                        <br>Contact Jodi Eggleton
                        <br>478 Neerim Road Murrumbeena VIC3168
                        <br>Telephone (03) 9568 7615
                        <br>mossandmapleflora@gmail.com
                        <br>
                        <a href="https://www.mossandmaple.com.au" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>
            <div class="row topspace-2x">
                <div class="col-sm-6 col-sm-push-6 text-center"><img src="../webroot/img/sponsors/rayWhite-1.jpg" alt=""></div>
                <div class="col-sm-6 col-sm-pull-6 paddingall bottomspace-xs">
                    <h4>Ray White</h4>
                    <p>
                        Considering selling or leasing?
                        <br><i>For a no obligation market appraisal on your hom or investment properties.</i>
                        <br>Call <b>Josh Hommelhoff</b> 0405 383 294
                        <br>Ray White Carnegie | 71 Koornang Road, Carnegie
                        <br>
                        <a href="https://raywhitecarnegie.com.au" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>
            <div class="row topspace-2x">
                <div class="col-sm-6 text-center"><img src="../webroot/img/sponsors/rosstown-1.jpg" alt=""></div>
                <div class="col-sm-6 paddingall">
                    <h4>The Rosstown Hotel</h4>
                    <p>
                        Bistro, Sports Bar, Sports Beer Garden, Front Bar, Front Bar Beer Garden, Function Rooms, Bottle Shop.
                        <br>1084 Dandenong Road, Carnegie
                        <br>9571 1033
                        <br>
                        <a href="https://www.rosstown.com.au" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>
            <div class="row topspace-2x">
                <div class="col-sm-6 col-sm-push-6 text-center"><img src="../webroot/img/sponsors/smartHire-1.png" alt=""></div>
                <div class="col-sm-6 col-sm-pull-6 paddingall bottomspace-xs">
                    <h4>Smart Hire</h4>
                    <p>
                        Contact <b>Paul Quearney</b> at
                        <br>Telephone (03) 9571 9488
                        <br>Fax (03) 9572 1565
                        <br>1076 Dandenong Road, Carnegie, VIC3163
                        <br>email: hire@carnegierental.com.au
                        <br>
                        <a href="https://www.carnegierental.com.au" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>
            <div class="row topspace-2x">
                <div class="col-sm-6 text-center"><img src="../webroot/img/sponsors/thomson-1.jpg" alt=""></div>
                <div class="col-sm-6 paddingall">
                    <h4>Thomson</h4>
                    <p>
                        Thinking of selling or leasing property?
                        <br>Contact David Thomson 0418 337 366
                        <br>
                        <a href="https://tre.com.au" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>
            <div class="row topspace-2x">
                <div class="col-sm-6 col-sm-push-6 text-center"><img src="../webroot/img/sponsors/woodards-1.png" alt=""></div>
                <div class="col-sm-6 col-sm-pull-6 paddingall bottomspace-xs">
                    <h4>Woodards</h4>
                    <p>
                        <i>Who sells mre property in 3163 than anyone else?</i>
                        <br>It's simple. We get results.
                        <br>Call Ruth Roberts 0409 214 110
                        <br>1148 Glenhuntly Road, Glenhuntly
                        <br>
                        <a href="https://www.woodards.com.au/office/carnegie" target="_blank">More about them &rarr;</a>
                    </p>
                </div>
            </div>

		</div>
	</section>
	<!-- end of FAQ -->


	<!-- CTA section -->
<!--	<section class="section section-cta jumbotron topspace-3x bottomspace-0">-->
<!--		<div class="container">-->
<!--			<h2>Get started-->
<!--				<span>Start your free, 30 day trial today!</span>-->
<!--			</h2>-->
<!--			<div class="row topspace-2x">-->
<!--				<div class="col-sm-9">-->
<!--					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat velit voluptatem quae tempora blanditiis sunt fugit facere praesentium vero, est natus, id accusantium ab animi voluptatibus magnam laborum ad nobis nam assumenda dolores quod cumque!</p>-->
<!--				</div>-->
<!--				<div class="col-sm-3"><a href="" class="btn btn-block btn-action">Start free trial</a></div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</section>-->
	<!-- end of CTA -->

<!--	<footer id="footer" class="clearfix">-->
<!---->
<!--		<div class="footer1">-->
<!--			<div class="container">-->
<!--				<div class="row">-->
<!---->
<!--					<div class="col-md-3 widget">-->
<!--						<h3 class="widget-title">Contact</h3>-->
<!--						<div class="widget-body">-->
<!--							<p>+234 23 9873237<br>-->
<!--								<a href="mailto:#">some.email@somewhere.com</a><br>-->
<!--								<br>-->
<!--								234 Hidden Pond Road, Ashland City, TN 37015-->
<!--							</p>-->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="col-md-3 widget">-->
<!--						<h3 class="widget-title">Follow us</h3>-->
<!--						<div class="widget-body">-->
<!--							<p class="follow-me-icons">-->
<!--								<a href=""><i class="fa fa-twitter fa-2"></i></a>-->
<!--								<a href=""><i class="fa fa-dribbble fa-2"></i></a>-->
<!--								<a href=""><i class="fa fa-github fa-2"></i></a>-->
<!--								<a href=""><i class="fa fa-facebook fa-2"></i></a>-->
<!--							</p>-->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="col-md-6 widget">-->
<!--						<h3 class="widget-title">Text widget</h3>-->
<!--						<div class="widget-body">-->
<!--							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, dolores, quibusdam architecto voluptatem amet fugiat nesciunt placeat provident cumque accusamus itaque voluptate modi quidem dolore optio velit hic iusto vero praesentium repellat commodi ad id expedita cupiditate repellendus possimus unde?</p>-->
<!--							<p>Eius consequatur nihil quibusdam! Laborum, rerum, quis, inventore ipsa autem repellat provident assumenda labore soluta minima alias temporibus facere distinctio quas adipisci nam sunt explicabo officia tenetur at ea quos doloribus dolorum voluptate reprehenderit architecto sint libero illo et hic.</p>-->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--				</div> <!-- /row of widgets -->
<!--			</div>-->
<!--		</div>-->

<!--		<div class="footer2">-->
<!--			<div class="container">-->
<!--				<div class="row">-->
<!---->
<!--					<div class="col-md-6 widget">-->
<!--						<div class="widget-body">-->
<!--							<p class="simplenav">-->
<!--								<a href="#">Home</a> |-->
<!--								<a href="about.html">About</a> |-->
<!--								<a href="sidebarright.ctp">Sidebar</a> |-->
<!--								<a href="contact.ctp">Contact</a> |-->
<!--								<b><a href="signup.html">Sign up</a></b>-->
<!--							</p>-->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="col-md-6 widget">-->
<!--						<div class="widget-body">-->
<!--							<p class="text-right">-->
<!--								Copyright &copy; 2014, Your name. Design: <a href="http://gettemplate.com/" rel="designer">GetTemplate</a>-->
<!--							</p>-->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--				</div> <!-- /row of widgets -->
<!--			</div>-->
<!--		</div>-->
<!--	</footer>-->


	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
	<script>
		jQuery(document).ready(function($) {
			$('#soft-illustration img').hover( function(){
				$('#soft-illustration').addClass('expand');
			}, function(){
				$('#soft-illustration').removeClass('expand');
			} );
		});
	</script>
</body>
</html>
