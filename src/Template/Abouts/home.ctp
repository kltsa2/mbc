<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\About[]|\Cake\Collection\CollectionInterface $abouts
 */
$this->layout = 'frontend';
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="The about us page of MBC">
    <meta name="author"      content="Dawood Kusdian">

    <title>About - Progressus Bootstrap template</title>
</head>

<body class="page-about">

<header class="head head-default" style ="background-image:url('../webroot/img/dandelion.jpg'); ">
    <div class="container">
        <div class="row">
            <h1 class="page-title text-center">About Us</h1>
            <h2 class="page-lead text-center">Meet the first bullsh*t-free premium Bootstrap template<br>made exclusively for outstanding companies.</h2>
        </div>
    </div>
</header>
<!-- end of header -->
<section class="section section-testimonials jumbotron">
    <div class="container">
        <h2 class="title text-center topspace">History</h2>
        <div id="testimonials-carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#testimonials-carousel" data-slide-to="1"></li>
                <li data-target="#testimonials-carousel" data-slide-to="2"></li>
            </ol>
            <!--//carousel-indicators-->
            <div class="carousel-inner">
                <div class="row item active">
                    <div class="col-sm-3 profile">
                        <img src="../webroot/img/img1.jpg" alt="" />
                    </div>
                    <div class="col-sm-9 content">
                        <blockquote>
                            <i class="fa fa-quote-left"></i>
                            <p>History 1</p>
                        </blockquote>
                        <p class="source">Mario Hayes
                            <br/><span class="title">Co-Founder, Superawesome Inc</span>
                        </p>
                    </div>
                    <!--//content-->
                </div>
                <!--//item-->
                <div class="row item">
                    <div class="col-sm-3 profile">
                        <img src="../webroot/img/img2.jpg" alt="" />
                    </div>
                    <div class="col-sm-9 content">
                        <blockquote>
                            <i class="fa fa-quote-left"></i>
                            <p>History2</p>
                        </blockquote>
                        <p class="source">2
                            <br/><span class="title">2</span>
                        </p>
                    </div>
                    <!--//content-->
                </div>
                <!--//item-->
                <div class="row item">
                    <div class="col-sm-3 profile">
                        <img src="../webroot/img/img3.jpg" alt="" />
                    </div>
                    <div class="col-sm-9 content">
                        <blockquote>
                            <i class="fa fa-quote-left"></i>
                            <p>History3</p>
                        </blockquote>
                        <p class="source">3
                            <br/><span class="title">3</span>
                        </p>
                    </div>
                    <!--//content-->
                </div>
                <!--//item-->
            </div>
            <!--//carousel-inner-->
        </div>
        <!--//carousel-->
    </div>
</section>
<!-- mission section -->
<section class="section-team topspace">
    <div class="container">
        <h2 class="text-center title">Goals and Achievements</h2>
        <hr>
        <div class="row">
            <div class="col-sm-10 col-sm-push-1">
                <p class="lead text-center">Our mission is... lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis odit beatae, consequatur adipisci quisquam minima atque, illo, impedit hic obcaecati sed sunt asperiores maiores aperiam aliquid.</p>
            </div>
        </div>
    </div>
</section>

<!-- end of mission section -->


<!-- History section -->
<section class="section-map jumbotron invert topspace-2x">
    <div class="container">
        <h2 class="text-center title">Learn more about us</h2>
        <img class="center-block" src="../webroot/img/dandelion2.png">
    </div>
</section>


<div class="container">
    <h2 class="text-center title">History</h2>
    <div class="row">
        <div class="col-sm-10 col-sm-push-1">
            <p class="lead medium-only-text-left">  Mr A.W.P. Olsen, a local real estate agent began inviting people to play bowls at “The Green”, a four rink bowling green attached to his home “Oakdene” at 41 Murrumbeena Road, Murrumbeena in the early 1920s.
                As interest in playing bowls grew, 36 mainly local residents held a meeting on 12 May 1923 at “The Green” to form the Murrumbeena Bowls Club. The Club’s first Annual General Meeting was held on 5 August 1924.
                Initially bowls continued to be played at the “The Green” but as membership grew, Mr Galt, one of the Club founders offered to purchase two suitable blocks of land for £770 at the current Blackwood Street site. In 1925 the green was completed and by 1927, the Clubhouse had been built at a cost of £1100.
                By the late 1980s the grass green had been in continuous use for over 60 years. So in 1990 Murrumbeena Bowls Club became one of the first Victorian bowls clubs to install a synthetic green. We liked this type of green so much that by 2014, we had installed our third synthetic green. What’s not to like – we play all year round even under lights.
                From its earliest beginnings, the Murrumbeena Bowls Club has had a strong sense of community partnership. It has also been a popular social hub, a tradition that continues today.
                We welcome everyone to come and experience what we have to offer.
                Be surprised. Bowls is more than a sport.</p>
        </div>
    </div>
</div>


<!-- team section -->
<section class="section-team topspace" style="margin-bottom: 10em;">
    <div class="container">

        <h2 class="text-center title">Meet the team</h2>
        <hr>

        <!-- team - row 1 -->
        <div class="row topspace-2x">
            <div class="col-md-6 bottomspace-xs">
                <div class="row">
                    <div class="col-sm-3 text-center-xs">
                        <a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/1.jpg"></a>
                    </div>
                    <div class="col-sm-9 text-center-xs">
                        <h4>
                            Jason Soto
                            <span class="person-social"><a href=""><i class="fa fa-twitter"></i> @jsoto34</a></span>
                        </h4>

                        <p>Jason ipsum dolor sit amet, consectetur adipisicing elit. Atque, cupiditate, numquam. Libero esse odio, laborum, fugiat quis facilis error distinctio provident porro inventore ullam atque vel aut, omnis veritatis dicta.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 bottomspace-xs">
                <div class="row">
                    <div class="col-sm-3 text-center-xs"><a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/2.jpg"></a> </div>
                    <div class="col-sm-9 text-center-xs">
                        <h4>
                            Brandy Cooper
                            <span class="person-social"><a href=""><i class="fa fa-twitter"></i> @bbbc2</a></span>
                        </h4>
                        <p>Brandy  non qui ipsum, natus quia. Minima et ex iure incidunt nihil accusantium, repudiandae possimus nulla! Placeat vel eos sequi nesciunt deserunt provident dignissimos voluptate, amet odio veritatis ipsam similique!</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row -->

        <!-- team - row 2 -->
        <div class="row topspace-2x">
            <div class="col-md-6 bottomspace-xs">
                <div class="row">
                    <div class="col-sm-3 text-center-xs">
                        <a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/4.jpg"></a>
                    </div>
                    <div class="col-sm-9 text-center-xs">
                        <h4>
                            Lucas Lawrence
                            <span class="person-social"><a href=""><i class="fa fa-twitter"></i> @llence</a></span>
                        </h4>
                        <p>Lucas saepe esse at obcaecati excepturi cumque tempore temporibus, nam magni aspernatur dolorum, nemo error velit expedita quia dolore consequuntur eos, animi ullam ut deserunt quibusdam aliquam, ex. Optio, molestiae.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 bottomspace-xs">
                <div class="row">
                    <div class="col-sm-3 text-center-xs"><a href="#"><img class="photo img-responsive img-circle" src="assets/images/people/3.jpg"></a> </div>
                    <div class="col-sm-9 text-center-xs">
                        <h4>
                            Alan Butler
                            <span class="person-social"><a href=""><i class="fa fa-twitter"></i> @alanb_</a></span>
                        </h4>
                        <p>Alan doloribus ducimus, incidunt nostrum consequatur ipsum aperiam architecto veritatis tempora dignissimos quos pariatur illo quasi molestias nesciunt, labore. Numquam ipsam blanditiis eius, tempora ab eum provident consequatur dolor vero</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row -->

    </div>
</section>
<!-- end of team section -->

</body>
</html>
