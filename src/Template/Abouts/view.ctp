<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\About $about
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit About'), ['action' => 'edit', $about->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete About'), ['action' => 'delete', $about->id], ['confirm' => __('Are you sure you want to delete # {0}?', $about->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Abouts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New About'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="abouts view large-9 medium-8 columns content">
    <h3><?= h($about->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Image History') ?></th>
            <td><?= h($about->image_history) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Background Image Url') ?></th>
            <td><?= h($about->background_image_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('History Image Url') ?></th>
            <td><?= h($about->history_image_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($about->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('History') ?></h4>
        <?= $this->Text->autoParagraph(h($about->history)); ?>
    </div>
    <div class="row">
        <h4><?= __('Goal') ?></h4>
        <?= $this->Text->autoParagraph(h($about->goal)); ?>
    </div>
    <div class="row">
        <h4><?= __('History Header') ?></h4>
        <?= $this->Text->autoParagraph(h($about->history_header)); ?>
    </div>
    <div class="row">
        <h4><?= __('Goal Header') ?></h4>
        <?= $this->Text->autoParagraph(h($about->goal_header)); ?>
    </div>
</div>
