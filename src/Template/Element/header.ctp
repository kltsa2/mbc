<div class= "navbar navbar-dual navbar-inverse navbar-fixed-top headroom ontop-now" id = "navbar">
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="index.ctp">
                <!--                <img src="../webroot/img/logos/test-1.jpg" alt="Progressus HTML5 template">-->
                <!--                <img src="../webroot/img/logos/mbc.png" alt="Progressus HTML5 template" class="secondary">-->
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li><a href="index">Home</a></li>
                <li><a href="../abouts/home">About</a></li>
                <li><a href="venue">Venue</a></li>
                <li><a href="members">Members</a></li>
                <li><a href="../sponsors/home">Sponsors</a></li>
                <li><a href="contact">Contact</a></li>
                <li><a class="btn btn-rounded" href="signin.html">SIGN IN</a></li>
            </ul>
        </div>
    </div>
</div>