

<?= $this->Html->charset() ?>
<?= $this->Html->meta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0']) ?>
<title>
    <?= $cakeDescription ?>
    <?= $this->fetch('title') ?>
</title>
<?= $this->Html->meta('icon') ?>


<?= $this->Html->css('bootstrap.min') ?>
<?= $this->Html->css('fonts-awesome.min') ?>
<?= $this->Html->css('styles') ?>
<?= $this->Html->script('html5shiv')?>
<?= $this->Html->script('respond.min')?>

<?= $this->fetch('meta') ?>
<?= $this->fetch('css') ?>

<?= $this->Html->script('jquery-3.2.1') ?>
<?= $this->Html->script('html5shiv') ?>
<?= $this->Html->script('respond.min') ?>
<?= $this->Html->script('bootstrap.min')?>
<?= $this->Html->script('bootstrap-hover-dropdown.min')?>
<?= $this->Html->script('headroom.min') ?>
<?= $this->Html->script('jQuery.headroom.min') ?>
<?= $this->Html->script('template') ?>
<?= $this->Html->script('imagesloaded.pkgd.min') ?>
<?= $this->Html->script('isotope.pkgd.min') ?>
<?= $this->Html->script('main') ?>
<?= $this->fetch('script') ?>

